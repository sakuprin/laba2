package edu.laba2.myapplication.expansion

import android.content.res.Configuration
import android.view.View
import android.widget.CompoundButton
import android.widget.RadioButton
import android.widget.Switch
import android.widget.Toast
import edu.laba2.myapplication.MainActivity
import java.util.*


internal val MainActivity.clearCheck: (CompoundButton, Boolean) -> Unit
    get() = { compoundButton: CompoundButton, b: Boolean ->
        secondRadioGroup.clearCheck()
        firstRadioGroup.clearCheck()
        currentRadioButton = compoundButton as RadioButton
    }
internal val MainActivity.updateLocale: (View) -> Unit
    get() = {
        val locale = Locale(if (it is Switch && it.isChecked) "ru" else "en")
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        resources.updateConfiguration(config, resources.displayMetrics)
        Toast.makeText(this, "Выполняется обновление для ${locale.toString()}", Toast.LENGTH_SHORT).show();
        updateText()
    }