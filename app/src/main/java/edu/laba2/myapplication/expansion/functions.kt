package edu.laba2.myapplication.expansion

import android.widget.*
import edu.laba2.myapplication.MainActivity
import edu.laba2.myapplication.R
import edu.laba2.myapplication.converter.Converter
import java.math.BigDecimal
import java.math.RoundingMode

fun MainActivity.initField() {
    topRadioGroup = get<RadioGroup>(R.id.topRadioGroup)
    firstRadioGroup = get<RadioGroup>(R.id.firstRadioGroup)
    secondRadioGroup = get<RadioGroup>(R.id.secondRadioGroup)
    app_сантиметры = get<RadioButton>(R.id.app_сантиметры)
    app_метры = get<RadioButton>(R.id.app_метры)
    app_километры = get<RadioButton>(R.id.app_километры)
    app_дюймы = get<RadioButton>(R.id.app_дюймы)
    app_мили = get<RadioButton>(R.id.app_мили)
    app_футы = get<RadioButton>(R.id.app_футы)
}

fun MainActivity.initListeners() {
    app_сантиметры.setOnCheckedChangeListener (clearCheck)
    app_метры.setOnCheckedChangeListener (clearCheck)
    app_километры.setOnCheckedChangeListener (clearCheck)
    app_дюймы.setOnCheckedChangeListener (clearCheck)
    app_мили.setOnCheckedChangeListener (clearCheck)
    app_футы.setOnCheckedChangeListener (clearCheck)
    get<Switch>(R.id.switch1).setOnClickListener(updateLocale)
    get<EditText>(R.id.editText).setOnClickListener({
        try {
            val editText = it as EditText
            if (editText.text == null || editText.text.isEmpty()) return@setOnClickListener

            val centimeters = Converter.values().
                    filter { it.id == currentRadioButton!!.id }.
                    first().
                    from(editText.text.toString().toDouble())
            Converter.values().forEach {
                val converterView = findViewById(it.viewId)as TextView
                converterView.text = BigDecimal(it.to(centimeters)).setScale(3, RoundingMode.UP).toDouble().toString()
            }
        } catch(e: Exception) {
            Toast.makeText(this, e.message, Toast.LENGTH_SHORT).show()
        }
    })
}


fun MainActivity.updateText() {
    app_сантиметры.text = resources.getText(R.string.app_сантиметры)
    app_метры.text = resources.getText(R.string.app_метры)
    app_километры.text = resources.getText(R.string.app_километры)
    app_дюймы.text = resources.getText(R.string.app_дюймы)
    app_мили.text = resources.getText(R.string.app_мили)
    app_футы.text = resources.getText(R.string.app_футы)

    get<Switch>(R.id.switch1).text = resources.getText(R.string.app_switch)
    get<TextView>(R.id.textView_app_дюймы).text = resources.getText(R.string.app_дюймы)
    get<TextView>(R.id.textView_app_километры).text = resources.getText(R.string.app_километры)
    get<TextView>(R.id.textView_app_метры).text = resources.getText(R.string.app_метры)
    get<TextView>(R.id.textView_app_мили).text = resources.getText(R.string.app_мили)
    get<TextView>(R.id.textView_app_сантиметры).text = resources.getText(R.string.app_сантиметры)
    get<TextView>(R.id.textView_app_футы).text = resources.getText(R.string.app_футы)
}

fun <T> MainActivity.get(id: Int) = findViewById(id)as T
