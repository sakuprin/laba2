package edu.laba2.myapplication

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.RadioButton
import android.widget.RadioGroup
import edu.laba2.myapplication.expansion.initField
import edu.laba2.myapplication.expansion.initListeners
import edu.laba2.myapplication.expansion.updateText

class MainActivity : AppCompatActivity() {
    internal var currentRadioButton: RadioButton? = null
    internal lateinit var topRadioGroup: RadioGroup
    internal lateinit var firstRadioGroup: RadioGroup
    internal lateinit var secondRadioGroup: RadioGroup
    internal lateinit var app_сантиметры: RadioButton
    internal lateinit var app_метры: RadioButton
    internal lateinit var app_километры: RadioButton
    internal lateinit var app_дюймы: RadioButton
    internal lateinit var app_мили: RadioButton
    internal lateinit var app_футы: RadioButton
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        initField()
        initListeners()
        updateText()
    }


}

