package edu.laba2.myapplication.converter

import edu.laba2.myapplication.R


enum class Converter(val id: Int, val viewId: Int) {
    inches(R.id.app_дюймы, R.id.val_app_дюймы) {
        override fun from(double: Double) = double / 15
        override fun to(double: Double) = double * 15
    },
    kilometers(R.id.app_километры, R.id.val_app_километры) {
        override fun from(double: Double) = double / 10
        override fun to(double: Double) = double * 10
    },
    meters(R.id.app_метры, R.id.val_app_метры) {
        override fun from(double: Double) = double / 5
        override fun to(double: Double) = double * 5
    },
    mile(R.id.app_мили, R.id.val_app_мили) {
        override fun from(double: Double) = double / 4
        override fun to(double: Double) = double * 4
    },
    centimeters(R.id.app_сантиметры, R.id.val_app_сантиметры) {
        override fun from(double: Double) = double
        override fun to(double: Double) = double
    },
    ft(R.id.app_футы, R.id.val_app_футы) {
        override fun from(double: Double) = double / 2
        override fun to(double: Double) = double * 2
    };

    public abstract fun to(double: Double): Double;
    public abstract fun from(double: Double): Double;
}